<?php namespace DaanHenke\Prefabs\Components;

use Cms\Classes\ComponentBase;
use DaanHenke\Prefabs\Models\Gallery as GalleryModel;

class Gallery extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'daanhenke.prefabs::lang.components.gallery.name',
            'description' => 'daanhenke.prefabs::lang.components.gallery.description'
        ];
    }

    public function defineProperties()
    {
        
        return [
            'gallery' => [
                'title' => 'daanhenke.prefabs::lang.components.gallery.properties.gallery.title',
                'type' => 'dropdown',
                'options' => GalleryModel::getList()
            ]
        ];
    }

    public function images(): array
    {
        $id = $this->property('gallery');
        
        if ($id === null)
        {
            return [];
        }

        $model = GalleryModel::where('id', (int) $id)->first();
        if ($model !== null)
        {
            return $model->images->toArray();
        }

        return [];
    }
}