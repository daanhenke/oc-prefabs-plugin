<?php namespace DaanHenke\Prefabs\Models;

use Model;

class Gallery extends Model
{
    public $table = 'daanhenke_prefabs_galleries';

    protected $guarded = ['*'];

    protected $fillable = [];

    public $attachMany = [
        'images' => 'System\Models\File'
    ];

    public static function getList(): array
    {
        $raw = self::all()->toArray();
        $out = [];

        foreach($raw as $entry)
        {
            $out[$entry["id"]] = $entry["name"];
        }

        return $out;
    }
}
