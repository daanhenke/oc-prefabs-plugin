<?php namespace DaanHenke\Prefabs;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'DaanHenke\Prefabs\Components\Gallery' => 'Gallery'
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'DaanHenke\Prefabs\FormWidgets\SubForm' => 'subform'
        ];
    }

    public function registerPermissions()
    {
        return [
            'daanhenke.prefabs.access_galleries' => [
                'label' => 'daanhenke.prefabs::lang.permissions.access_galleries.label',
                'tab' => 'Prefabs',
                'order' => 200
            ]
        ];
    }
}