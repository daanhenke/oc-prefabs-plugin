<?php namespace DaanHenke\Prefabs\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Backend\Widgets\Form;

class SubForm extends FormWidgetBase
{
    protected $defaultAlias = 'subform';

    protected $formWidget;
    protected $fields;

    public $form;

    public function init()
    {
        $this->fillFromConfig([
            'form'
        ]);

        if ($this->formField->disabled)
        {
            $this->previewMode = true;
        }

        $config = $this->makeConfig($this->form);
        $config->model = $this->model;
        $config->data = $this->getLoadValue();
        $config->alias = $this->alias . $this->defaultAlias;
        $config->arrayName = $this->getFieldName();
        $config->isNested = true;

        if (object_get($this->getParentForm()->config, 'enableDefaults') === true)
        {
            $config->enableDefaults = true;
        }

        $this->formWidget = $this->makeWidget(Form::class, $config);
        $this->formWidget->previewMode = $this->previewMode;
        $this->formWidget->bindToController();

        $this->fields = $this->formWidget->getFields();
    }

    public function render()
    {
        $rows = [];
        $current_row = [];
        $current_row_type = "unknown";

        foreach ($this->fields as $field)
        {
            $column = isset($field->config["column"]) ? $field->config["column"] : "full";
            $column_type = $column === "full" ? "full" : "lr";

            if ($current_row_type !== $column_type)
            {
                if ($current_row_type !== "unknown")
                {
                    $rows[] = $current_row;
                }

                if ($column_type === "full")
                {
                    $current_row = [
                        "full" => []
                    ];

                    $current_row_type = "full";
                }
                else
                {
                    $current_row = [
                        "left" => [],
                        "right" => []
                    ];

                    $current_row_type = "lr";
                }
            }
            
            $current_row[$column][] = $field;
        }

        $rows[] = $current_row;

        return $this->makePartial('index', [
            "rows" => $rows,
            "widget" => $this->formWidget
        ]);
    }
}